from django.apps import AppConfig


class DealbookConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dealbook'
