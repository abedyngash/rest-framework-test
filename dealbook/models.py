from django.db import models

# Create your models here.


class Supplier(models.Model):
    """
    The Supplier model holds the profiles for all registered suppliers
    -> business_name
    -> email_address
    -> phone_number
    -> business_registration_number
    -> business_category
    """
    pass


class Customer(models.Model):
    """
    The Customer model holds the details for the various customers related to different suppliers
    -> name
    -> email
    -> phone_number
    """
    pass


class Requisition(object):
    """
    The Requisition model holds the Requisitions
    -> requisition_number
    -> added_by
    -> description
    -> quantity
    -> status
    -> value
    -> requisition_date
    -> procurement_method
    """
    pass


class Deal(models.Model):
    """
    The Deal model holds deals from a given customer to a given supplier
    -> supplier
    -> customer
    -> requisition
    -> order_number
    -> product
    -> delivery_date
    -> amount
    """
    pass


class Milestone(models.Model):
    """
    The Milestone model holds all the milestones for the various deals
    -> deal
    -> description
    -> date
    """
    pass
